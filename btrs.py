#!/usr/bin/env python3

import sys
import os
import struct
import socket
import argparse
import platform
import subprocess
import string

# Basic troubleshooting script
# Supports OpenBSD, macOS, Linux
# No IPv6 Support.
# Script checks some default locations for iptables, if it is somewhere else you need to adjust it
# Traceroute gets executed last when perfoming multiple action so you can ctrl-c it wihtout stopping progress of other checks 
# Version 1.0.0
# Rolf Risiko

# Functions

def ping_host(pinghost):
    if validate_ip(pinghost) == True:
        try:
            subprocess.check_call(
                ['ping', '-c', '2', pinghost],
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL
            )
            print("[+] %s is reachable via ping" % (pinghost))
        except subprocess.CalledProcessError:
            print("[-] %s is NOT reachable via ping" % (pinghost))
    else:
        phost = socket.gethostbyname(pinghost)
        try:
            subprocess.check_call(
                ['ping', '-c', '2', phost],
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL
            )
            print("[+] %s is reachable via ping" % (phost))
        except subprocess.CalledProcessError:
            print("[-] %s is NOT reachable via ping" % (phost))

def try_tcp_connect(tcphost, tcpport, tcptimeout):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(int(tcptimeout))
    try:
        s.connect((tcphost, int(tcpport)))
        print("[+] %s:%s is reachable via TCP" % (tcphost, tcpport))
    except Exception as e:
        print("[-] TCP connect to %s:%s failed. Error was: %s" % (tcphost, tcpport, e))
    finally:
        s.close()

def local_fw_ruleset():
    if os.getuid() != 0:
        print("[!] Not running as root, skipping local firewall ruleset.")
    else:
        if platform.system() == 'Darwin':
            if os.path.exists('/etc/pf.conf'):
                print('[+] /etc/pf.conf (Please check anchors manually): ')
                for line in open('/etc/pf.conf', 'r'):
                    li = line.strip()
                    if not li.startswith('#'):
                        print(line.rstrip())
                print("")
            else: 
                print("[-] Could not find PF ruleset")
        if platform.system() == 'OpenBSD':
            if os.path.exists('/etc/pf.conf'):
                print('[+] /etc/pf.conf (Please check anchors manually): ')
                for line in open('/etc/pf.conf', 'r'):
                    li = line.strip()
                    if not li.startswith('#'):
                        print(line.rstrip())
                print("")
            else: 
                print("[-] Could not find PF ruleset")
        if platform.system() == 'Linux':
            if os.path.exists('/etc/iptables.conf'):
                for line in open('/etc/iptables.conf', 'r'):
                    li = line.strip()
                    if not li.startswith('#'):
                        print(line.rstrip())
            if os.path.exists('/etc/iptables/rules'):
                for line in open('/etc/iptables/rules', 'r'):
                    li = line.strip()
                    if not li.startswith('#'):
                        print(line.rstrip())
            if os.path.exists('/etc/iptables/iptables.conf'):
                for line in open('/etc/iptables/iptables.conf', 'r'):
                    li = line.strip()
                    if not li.startswith('#'):
                        print(line.rstrip())
            if os.path.exists('/etc/iptables/iptables.v4'):
                for line in open('/etc/iptables/iptables.v4', 'r'):
                    li = line.strip()
                    if not li.startswith('#'):
                        print(line.rstrip())
            if os.path.exists('/etc/iptables/rules.v4'):
                for line in open('/etc/iptables/rules.v4', 'r'):
                    li = line.strip()
                    if not li.startswith('#'):
                        print(line.rstrip())
            else:
                print("[-] Could not find ruleset file")
            print("")

def traceroute(traceroutehost):
    if os.getuid() != 0:
        print("[!] Not running as root, skipping traceroute.")
    print("[+] traceroute to %s" % traceroutehost)
    dest_addr = traceroutehost
    port = 33434
    max_hops = 30
    ttl = 1
    while ttl <= max_hops:
        rec_socket = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_ICMP)
        send_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        send_socket.setsockopt(socket.SOL_IP, socket.IP_TTL, ttl)
        timeout = struct.pack("ll", 5, 0)
        rec_socket.setsockopt(socket.SOL_SOCKET, socket.SO_RCVTIMEO, timeout)
        rec_socket.bind(("", port))
        sys.stdout.write(" %d   " % ttl)
        send_socket.sendto(bytes("", "utf-8"), (traceroutehost, port))
        curr_addr = None
        curr_name = None
        finished = False
        tries = 3
        error_count = 0
        while not finished and tries > 0:
            try:
                _, curr_addr = rec_socket.recvfrom(512)
                finished = True
                curr_addr = curr_addr[0]
                try:
                    curr_name = socket.gethostbyaddr(curr_addr)[0]
                except socket.error:
                    curr_name = curr_addr
            except Exception as err:
                tries -= 1
                error_count += 1
                sys.stdout.write("* ")
            #finally:
            #    if error_count > 8:
            #        break

        send_socket.close()
        rec_socket.close()
        if not finished:
            pass
        
        if curr_addr is not None:
            curr_host = "%s (%s)" % (curr_name, curr_addr)
        else:
            curr_host = ""
        sys.stdout.write("%s\n" % (curr_host))
        ttl += 1
        if curr_addr == dest_addr or ttl > max_hops:
            break

def get_routes(specifichost):
    if specifichost:
            if validate_ip(specifichost) == True:
                cmd = "ip r get " + specifichost
                print("[+] Route(s) to %s " % (specifichost))
                os.system(cmd)
            else:
                addr = socket.gethostbyname(specifichost)
                cmd = "ip r get " + addr
                print("[+] Route(s) to %s " % (specifichost))
                os.system(cmd)
    else:
        os.system('ip r')

def validate_ip(ipinput):
    splitip = ipinput.split('.')
    if len(splitip) != 4:
        return False
    for x in splitip:
        if not x.isdigit():
            return False
        i = int(x)
        if i < 0 or i > 254:
            return False
    return True

def resolve(resolvname):
    if validate_ip(resolvname) == True:
        print("[+] DNS resolver: input is a valid IP, trying to get PTR")
        try:
            PTR_record = socket.gethostbyaddr(resolvname)
            clean_PTR_record = PTR_record[0]
            print("[+] %s resolves to %s" % (resolvname, clean_PTR_record))
        except:
            print("[-] Could not resolve %s" % (resolvname))
    else: 
        print("[!] DNS resolver: input is not a valid IP, assuming hostname.")
        print("[+] DNS resolver: trying to get A record for %s" % (resolvname))
        try:
            A_record = socket.gethostbyname(resolvname)
            print("[+] %s resolves to %s" % (resolvname, A_record))
        except:
            print("[-] Could not resolve %s" % (resolvname))


# Input arguments
argumentparser = argparse.ArgumentParser(
    description="Simple troubleshooting script by Rolf Risiko. Order of parameters is not important.")
argumentparser.add_argument("-v", "--version", action="store_true",
                            help="show program version")
argumentparser.add_argument('-p', '--ping', action='store', dest='pinghost', 
                            help="Host to test ping")
argumentparser.add_argument('-H', '--tcphost', action='store', dest='tcphost',
                            help='Host for TCP check, use in combination with -P')
argumentparser.add_argument('-P', '--tcpport', action='store', dest='tcpport',
                            help='Host for TCP check, use in combination with -H')
argumentparser.add_argument('-T', '--timeout', action='store', dest='tcptimeout', default=5,
                            help='Seconds before timeout is raised when testing TCP. Default is 5. Use in combination wit -t and -P')
argumentparser.add_argument('-f', '--firewall', action='store_true', dest='iptables_boolean',
                            help='Print local firewall ruleset. Requires root permissions' )
argumentparser.add_argument('-t', '--traceroute', action='store', dest='tracehost',
                            help='Host to traceroute. Needs root permissions on macos.')
argumentparser.add_argument('-r', '--routes', action='store_true', dest='showroutes_boolean',
                            help='Print complete routingtable')
argumentparser.add_argument('-R', '--getspecificroute', action='store', dest='specificroutehost',
                            help='Show routes for specific host')
argumentparser.add_argument('-d', '--resolve', action="store", dest='resolvname',
                            help='Name or IP for DNS lookup')
argumentparser.add_argument('-V', '--validate', action='store', dest='iptovalidate',
                            help='Enter an IP to check if its valid')
argumentparser.add_argument('-a', '--checksinglehost', action='store', dest='singlehost',
                            help='Perform actions to single host (Can be combined with -P for TCP check)')

args = argumentparser.parse_args()
if args.version:
    print("BasicTRoubleshootingScript version 1.0.0")

# Doing

if args.pinghost:
    ping_host(str(args.pinghost))

if args.tcphost:
    if args.tcpport:
        try_tcp_connect(args.tcphost, args.tcpport, args.tcptimeout)

if args.showroutes_boolean:
    get_routes(None)

if args.specificroutehost:
    get_routes(args.specificroutehost)

if args.iptables_boolean:
    local_fw_ruleset()

if args.resolvname:
    resolve(args.resolvname)

if args.tracehost:
    traceroute(args.tracehost)

if args.iptovalidate:
    if validate_ip(args.iptovalidate):
        print("[+] %s is a valid IP address" % (args.iptovalidate))
    else:
        print("[-] %s is not a valid IP address" % (args.iptovalidate))

if args.singlehost:
    ping_host(str(args.singlehost))
    if args.tcpport:
        try_tcp_connect(args.singlehost, args.tcpport, args.tcptimeout)
    get_routes(args.singlehost)
    resolve(args.singlehost)
    traceroute(args.singlehost)