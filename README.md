# btrs

Basic TRoubleshooting Script

Usage:

````
usage: btrs.py [-h] [-v] [-p PINGHOST] [-H TCPHOST] [-P TCPPORT]
               [-T TCPTIMEOUT] [-f] [-t TRACEHOST] [-r] [-R SPECIFICROUTEHOST]
               [-d RESOLVNAME] [-V IPTOVALIDATE] [-a SINGLEHOST]

Simple troubleshooting script by Rolf Risiko. Order of parameters is not
important.

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program version
  -p PINGHOST, --ping PINGHOST
                        Host to test ping
  -H TCPHOST, --tcphost TCPHOST
                        Host for TCP check, use in combination with -P
  -P TCPPORT, --tcpport TCPPORT
                        Host for TCP check, use in combination with -H
  -T TCPTIMEOUT, --timeout TCPTIMEOUT
                        Seconds before timeout is raised when testing TCP.
                        Default is 5. Use in combination wit -t and -P
  -f, --firewall        Print local firewall ruleset. Requires root
                        permissions
  -t TRACEHOST, --traceroute TRACEHOST
                        Host to traceroute. Needs root permissions on macos.
  -r, --routes          Print complete routingtable
  -R SPECIFICROUTEHOST, --getspecificroute SPECIFICROUTEHOST
                        Show routes for specific host
  -d RESOLVNAME, --resolve RESOLVNAME
                        Name or IP for DNS lookup
  -V IPTOVALIDATE, --validate IPTOVALIDATE
                        Enter an IP to check if its valid
  -a SINGLEHOST, --checksinglehost SINGLEHOST
                        Perform actions to single host (Can be combined with
                        -P for TCP check)
````
